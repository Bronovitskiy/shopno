var gulp = require("gulp");
var sass = require("gulp-sass");

gulp.task("sass", function() {
    return gulp.src("app/scss/main.scss")
        .pipe(sass())
        .pipe(gulp.dest("app/css"))
        .pipe(browserSync.reload({
            stream: true
        }));
});

var hogan = require("gulp-hogan");

gulp.task("build-hogan", function(){
    gulp.src("app/templates/index.hogan", {}, ".html")
        .pipe(hogan(null, null, ".html"))
        .pipe(gulp.dest("app/"));
});

gulp.task("watch", ["browserSync", "sass"], function(){
    gulp.watch("app/scss/**/*.scss", ["sass"]);
    gulp.watch("app/*.html", browserSync.reload);
    gulp.watch("app/js/**/*.js", browserSync.reload);
    gulp.watch("app/templates/**/*.hogan", ["build-hogan",browserSync.reload]);
});

var browserSync = require("browser-sync").create();

gulp.task("browserSync", function() {
    browserSync.init({
        server: {
            baseDir: "app"
        }
    });
});

var uglify = require("gulp-uglify");
var gulpIf = require("gulp-if");
var useref = require("gulp-useref");
var cssnano = require("gulp-cssnano");

gulp.task("useref", function(){
    return gulp.src("app/*.html")
        .pipe(useref())
        .pipe(gulpIf("*.js", uglify()))
        .pipe(gulpIf("*.css", cssnano()))
        .pipe(gulp.dest("dist"));
});

var imagemin = require("gulp-imagemin");
var cache = require("gulp-cache");

gulp.task("images", function(){
    return gulp.src("app/images/**/*.+(png|jpg|jpeg|gif|svg)")
        .pipe(cache(imagemin({
            interlaced: true
        })))
        .pipe(gulp.dest("dist/images"));
});

gulp.task("fonts", function() {
    return gulp.src("app/fonts/**/*")
        .pipe(gulp.dest("dist/fonts"));
});

var del = require("del");

gulp.task("clean:dist", function() {
    return del.sync("dist");
});

gulp.task("cache:clear", function (callback) {
    return cache.clearAll(callback);
});

var runSequence = require("run-sequence");

gulp.task("build", function (callback) {
    runSequence(
        "build-hogan",
        "clean:dist",
        ["sass", "useref", "images", "fonts"],
        callback
    );
});

gulp.task("default", function (callback) {
    runSequence(["sass","browserSync", "watch"],
        callback
    );
});