var GoogleMap = function(divClass, geo) {
    var googleMapAria = document.querySelector(divClass + " .google-map");

    var map = new google.maps.Map(googleMapAria, {
        zoom: 4,
        center: geo
    });
    var marker = new google.maps.Marker({
        position: geo,
        map: map
    });
};