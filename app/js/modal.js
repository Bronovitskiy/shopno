var Modal = function(modal, btn) {
    this.modal = document.querySelector(modal);
    this.btn = document.querySelector(btn);
    this.addListeners(this);
};

Modal.prototype.show = function() {
    this.modal.style.display = "block";
};

Modal.prototype.close = function() {
    this.modal.style.display = "none";
};

Modal.prototype.addListeners = function(that) {
    if(this.btn) {
        this.btn.onclick = function () {
            that.show();
        };
    }

    window.onclick = function(event) {
        if (event.target == that.modal) {
            that.close();
        }
    };
};

