var MessageForm = function(formClass) {
    this.formClass = document.querySelector(formClass);
    this.addListeners(this)
};

MessageForm.prototype.addListeners = function(that) {
    this.formClass.onsubmit = function(event) {
        event.preventDefault();
        new Modal(".message-form-success-model", null).show();
    };
};