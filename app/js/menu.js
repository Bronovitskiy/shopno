var Menu = function() {
    this.isRender = false;
    this.menuButton = document.querySelector(".menu-button");
    this.mainMenu = document.querySelector(".main-menu");
    this.addListeners(this);
};

Menu.prototype.show = function() {
    this.isRender = !this.isRender;

    if(this.isRender) {
        this.menuButton.style.display = "none";
        this.mainMenu.style.display = "block";
    } else {
        this.menuButton.style.display = "block";
        this.mainMenu.style.display = "none";
    }
};

Menu.prototype.addListeners = function(that) {
    that.menuButton.onclick = function() {
        that.show();
    };

    that.mainMenu.onclick = function() {
        that.show();
    };
};