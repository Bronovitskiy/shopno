var Slider = function(sliderName) {
    this.slideIndex = 0;
    this.sliderName = sliderName;
    this.slides = document.querySelectorAll("." + this.sliderName + " .slider__item");
    this.dots = document.querySelectorAll("." + this.sliderName + " .point-group__point");
    this.addListeners(this);
};

Slider.prototype.defaultStatus = function() {
    for (var i = 0; i < this.slides.length; i++) {
        this.slides[i].style.display = "none";
        this.dots[i].classList.remove("point-group__point_active");
    }
};

Slider.prototype.setSlide = function() {
    this.slides[this.slideIndex - 1].style.display = "inline-block";
    this.dots[this.slideIndex - 1].classList += " point-group__point_active";
};

Slider.prototype.next = function() {
    this.defaultStatus();

    this.slideIndex++;

    if (this.slideIndex > this.slides.length) {
        this.slideIndex = 1;
    }

    this.setSlide();
};

Slider.prototype.prev = function() {
    this.defaultStatus();

    this.slideIndex--;

    if (this.slideIndex < 1) {
        this.slideIndex = this.slides.length;
    }

    this.setSlide();
};

Slider.prototype.start = function() {
    this.next();
    setInterval(this.next.bind(this), 10000);
};

Slider.prototype.addListeners = function(that) {
    var blockName = that.sliderName.substring(0, that.sliderName.indexOf('__'));
    var prevBtn = document.querySelector("." + blockName + "__prev");
    var nextBtn = document.querySelector("." + blockName +"__next");

    if(prevBtn && nextBtn) {
        prevBtn.onclick = function () {
            that.prev();
        };
        nextBtn.onclick = function () {
            that.next();
        };
    }
};
