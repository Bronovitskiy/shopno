function init() {
    new Slider("about-us__slider").start();
    new Slider("what-they-says__slider").start();
    new Menu();
    new Modal(".contact-us-model", ".header__contact-us");
    new GoogleMap(".modal__content", {lat: -25.363, lng: 131.044});
    new GoogleMap(".contact", {lat: -25.363, lng: 131.044});
    new MessageForm(".message-form")
}

window.onload = function() {
    init();
};